/* 
 * File:   main.h
 * Author: Esteban Naranjo
 *
 * Created on 23 de julio de 2021, 09:41 AM
 */

#ifndef MAIN_H
#define	MAIN_H

#ifdef	__cplusplus
extern "C" {
#endif
    
#include <plib.h>
    
#define LED_1 BIT_4
#define LED_2 BIT_6
#define LED_3 BIT_7

    
#define SYSCLK 96000000
#define FPBCLK (SYSCLK/2)
#define CORE_TICK_RATE (SYSCLK/2/1000)



#ifdef	__cplusplus
}
#endif

#endif	/* MAIN_H */

