
#include <xc.h>
#include <plib.h>

#include "main.h"
#include "core_timer.h"

void __ISR(_CORE_TIMER_VECTOR, IPL7_AUTO) core_timer_interrupt_handler(void)
{
    mCTClearIntFlag();
    UpdateCoreTimer(CORE_TICK_RATE);

    core_timer_increment_sys_tick();
}
