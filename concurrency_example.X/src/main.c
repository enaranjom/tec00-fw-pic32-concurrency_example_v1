/* 
 * File:   main.c
 * Author: Esteban Naranjo
 *
 * Created on 23 de julio de 2021, 09:38 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include <xc.h>
#include "main.h"
#include "config_bits.h"

#define HEARTBEAT_PERIOD 500 // ms



uint32_t sys_tick=0;

void core_timer_init(void);
void gpio_init(void);
void heartbeat_handler(void);

void main(void) {
    
    gpio_init();
    core_timer_init();
    
    /* Configure and enable the interrupts of the system */
    INTConfigureSystem(INT_SYSTEM_CONFIG_MULT_VECTOR);
    INTEnableInterrupts();
    
    
    while(1)
    {
        heartbeat_handler();
    }
}



void heartbeat_handler(void) 
{
    static uint32_t heartbeat_tick = 0;
    if ((sys_tick - heartbeat_tick) > HEARTBEAT_PERIOD) {
        heartbeat_tick = sys_tick;
        mPORTEToggleBits(LED_2);
    }
}

void core_timer_init(void)
{
    /* Set the period of the timer to 1ms */
    OpenCoreTimer(CORE_TICK_RATE);
    
    /* Configure the interruption of the core timer */
    mConfigIntCoreTimer((CT_INT_ON | CT_INT_PRIOR_7 | CT_INT_SUB_PRIOR_0));

}

void gpio_init(void)
{
    mPORTESetPinsDigitalOut(LED_1 | LED_2 | LED_3);
    mPORTEClearBits(LED_1 | LED_2 | LED_3);
}